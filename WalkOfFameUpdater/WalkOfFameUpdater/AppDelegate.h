//
//  AppDelegate.h
//  WalkOfFameUpdater
//
//  Created by Guillermo Zafra on 06/10/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

