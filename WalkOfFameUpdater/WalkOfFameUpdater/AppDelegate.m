//
//  AppDelegate.m
//  WalkOfFameUpdater
//
//  Created by Guillermo Zafra on 06/10/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import "AppDelegate.h"
#import "NSString+stripHtml.h"

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;
@property (nonatomic, strong) NSMutableArray *celebs;
@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
    
    self.celebs = [NSMutableArray new];
    
    NSDictionary *celebsAndAddresses = [self parseHtml];
    
    for (NSString *name in celebsAndAddresses.allKeys) {
        NSString *address = [celebsAndAddresses objectForKey:name];
        NSString *location = @"";
        
        NSMutableDictionary *celebDict = [NSMutableDictionary new];
        
        [celebDict setObject:name forKey:@"name"];
        [celebDict setObject:address forKey:@"address"];
        
        @synchronized(self.celebs){
            @try {
                location = [self getLocationForAddress:[NSString stringWithFormat:@"%@, Los Angeles",address]];
                
            }
            @catch (NSException *exception) {
                NSLog(@"Error getting lcoation for address %@",address);
            }
            @finally {
                [celebDict setObject:location forKey:@"location"];
            }
            
            [self.celebs addObject:celebDict];
        }
        
        NSLog(@"%@ Processed loc: %@",name,location);
        [NSThread sleepForTimeInterval:1.0f];
        NSLog(@"------ NEXT ------");

    }
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self.celebs
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"%@",jsonString);
    }
    
//    [self getLocationForAddress:[NSString stringWithFormat:@"6201 Hollywood Blvd %@",@"Walk of Fame"]];
    
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

- (NSString*)getLocationForAddress:(NSString*)address{
    NSString *locationString = @"";
    address = [address stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    
    NSString *url = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?address=%@",address];
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL  URLWithString:url]];
    
    [request setHTTPMethod:@"GET"];
    
    [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"content-type"];
    
    NSError *err;
    
    NSURLResponse *response;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request   returningResponse:&response error:&err];
    
    //You need to check response.Once you get the response copy that and paste in ONLINE JSON VIEWER.If you do this clearly you can get the correct results.
    
    //After that it depends upon the json format whether it is DICTIONARY or ARRAY
    if (!responseData || err) {
        NSLog(@"Error retrieving location %@",[err description]);
        return @"Not found WTF?!";
    }
    
    NSDictionary *jsonArray = [NSJSONSerialization JSONObjectWithData:responseData options: NSJSONReadingMutableContainers error: &err];
    
    NSArray *array=[jsonArray objectForKey:@"results"];
//    NSLog(@"%@",[array description]);
    
    if (array.count > 0) {
        id results = [array objectAtIndex:0];
        id geometry = [results objectForKey:@"geometry"];
        if (geometry) {
            id location = [geometry objectForKey:@"location"];
            locationString = [locationString stringByAppendingString:
                              [NSString stringWithFormat:@"%@,%@",
                               [location objectForKey:@"lat"],
                               [location objectForKey:@"lng"]]];
        }else{
            NSLog(@"Geometry not found for address %@",address);
        }
    }
    
   
    return locationString;
}

- (NSDictionary*)parseHtml{
    
    NSString *url = [NSString stringWithFormat:@"http://www.seeing-stars.com/Immortalized/WalkOfFameStars.shtml"];
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL  URLWithString:url]];
    
    [request setHTTPMethod:@"GET"];
    
    [request setValue:@"html/text;charset=UTF-8" forHTTPHeaderField:@"content-type"];
    
    NSError *err;
    
    NSURLResponse *response;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request   returningResponse:&response error:&err];
    
    //You need to check response.Once you get the response copy that and paste in ONLINE JSON VIEWER.If you do this clearly you can get the correct results.
    
    //After that it depends upon the json format whether it is DICTIONARY or ARRAY
    
    NSString *htmlString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    

//    NSRange range = [htmlString rangeOfString:@"<font face=\"arial\"><font color=\"#000000\">"];
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    
    BOOL firstChunk = YES;
    
    NSUInteger length = [htmlString length];
    NSRange range = NSMakeRange(0, length);
    while(range.location != NSNotFound)
    {
        range = [htmlString rangeOfString:@"<font face=\"arial\">" options:0 range:range];
        if(range.location != NSNotFound) {
            NSUInteger firstCharacterPosition = range.location + range.length;
            NSRange rangeOfNextKey = [htmlString rangeOfString:@"<font face=\"arial\">"
                                                       options:0
                                                         range:NSMakeRange(firstCharacterPosition, length-firstCharacterPosition)];
            if (rangeOfNextKey.location == NSNotFound) {
                break;
            }else{
                NSUInteger lastCharacterPosition = rangeOfNextKey.location;
                
                if (!firstChunk) {
                    NSString *foundString = [[htmlString substringToIndex:lastCharacterPosition] substringFromIndex:firstCharacterPosition];
                    
                    NSString *address = @"";
                    NSString *name = @"";
                    
                    @try {
                        address = [self getAddressFrom:foundString];
                        
                        NSString *key = [self string:address containsString:@"Street"] ? @"Street" : @"Blvd";
                        
                        NSRange rangeOfAddress = [foundString rangeOfString:address];
                        if (rangeOfAddress.location == NSNotFound) {
                            NSLog(@"Address not found in [%@] > Skipping",foundString);
                            range = NSMakeRange(firstCharacterPosition, length - firstCharacterPosition);
                            continue;
                        }
                        
                        foundString = [foundString substringFromIndex:rangeOfAddress.location + rangeOfAddress.length];
                        
                        //                    foundString = [foundString stringByReplacingOccurrencesOfString:address withString:@""];
                        
                        name = [self getNameFrom:foundString];
                        
                        [dict setObject:address forKey:name];
                    }
                    @catch (NSException *exception) {
                        NSLog(@"Exception: ADDRESS: %@ NAME: %@",address,name);
                    }
                    @finally {
                        
                    }
                }else{
                    firstChunk = NO;
                }
                
                range = NSMakeRange(firstCharacterPosition, length - firstCharacterPosition);
            }
        }
    }
    

    
    NSLog(@"%@",[dict description]);
    return [NSDictionary dictionaryWithDictionary:dict];
}


- (NSString*)getNameFrom:(NSString*)string{
    NSString *filtered = string;[self stringByStrippingHTML:string];
    
    BOOL openHtmlTag = NO;
    NSString *nameString = @"";
    for (int i = 0; i < filtered.length; i++) {
        NSRange range = NSMakeRange(i,1);
        NSString *charAtIdx = [filtered substringWithRange:range];
        if ([charAtIdx isEqualToString:@"<"]) {
            openHtmlTag = YES;
        }else if([charAtIdx isEqualToString:@">"]) {
            openHtmlTag = NO;
        }else{
            if (!openHtmlTag) {
                nameString = [nameString stringByAppendingString:charAtIdx];
            }
        }
    }

    nameString = [nameString stringByReplacingOccurrencesOfString:@"\r\n" withString:@" "];
    nameString = [nameString stringByReplacingOccurrencesOfString:@".: " withString:@""];
    nameString = [nameString stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@""];
    nameString = [nameString stringByTrimmingCharactersInSet:
                  [NSCharacterSet whitespaceCharacterSet]];
    nameString = [nameString stringByReplacingOccurrencesOfString:@"&amp;" withString:@""];
    nameString = [nameString stringByReplacingOccurrencesOfString:@".:" withString:@""];
    nameString = [nameString stringByReplacingOccurrencesOfString:@":" withString:@""];

    return nameString;
}

-(NSString *)stringByStrippingHTML:(NSString*)str
{
    BOOL openHtmlTag = NO;
    NSString *resultStr = @"";
    for (int i = 0; i < str.length; i++) {
        NSRange range = NSMakeRange(i,1);
        NSString *charAtIdx = [str substringWithRange:range];
        if ([charAtIdx isEqualToString:@"<"] || [charAtIdx isEqualToString:@"&"]) {
            openHtmlTag = YES;
        }else if([charAtIdx isEqualToString:@">"] || [charAtIdx isEqualToString:@";"]) {
            openHtmlTag = NO;
        }else{
            if (!openHtmlTag) {
                resultStr = [resultStr stringByAppendingString:charAtIdx];
            }
        }
    }
    return resultStr;
}

- (BOOL)string:(NSString*)aString containsString:(NSString*)key{
    if ([aString rangeOfString:key].location != NSNotFound) {
        return YES;
    }
    return NO;
}

- (NSString*)removeWordsIn:(NSArray*)wordsArray fromString:(NSString*)string{
    NSString *result = string;
    for (NSString *filterWord in wordsArray) {
        result = [result stringByReplacingOccurrencesOfString:filterWord withString:@""];
    }
    return result;
}

- (NSString*)getAddressFrom:(NSString*)aString{
    NSString *string = aString;//[self stringByStrippingHTML:aString];
    NSRange endRange = [string rangeOfString:@"Hollywood"];
    NSString *address = @"";
    if (endRange.location != NSNotFound) {
        address = [[string substringToIndex:endRange.location] substringFromIndex:endRange.location - 5];
        address = [address stringByAppendingString:@"Hollywood Blvd"];
    }else{
        endRange = [string rangeOfString:@"Vine"];
        if (endRange.location != NSNotFound) {
            address = [[string substringToIndex:endRange.location] substringFromIndex:endRange.location - 5];
            address = [address stringByAppendingString:@"Vine Street"];
        }else{
            NSLog(@"Address not found in %@",aString);
        }
    }
    return address;
}

@end
