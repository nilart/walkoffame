//
//  NSString+stripHtml.h
//  WalkOfFameUpdater
//
//  Created by Guillermo Zafra on 07/10/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (stripHtml)
- (NSString*)stripHtml;
@end
