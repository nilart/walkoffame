//
//  DataController.m
//  WalkOfFame
//
//  Created by Guillermo Zafra on 04/09/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import "WOFDataController.h"
#import "WOFCategory.h"
#import "WOFCelebrity.h"
#import "WOFDataManager.h"

@interface WOFDataController()

@end

@implementation WOFDataController

+ (instancetype)sharedController {
    static WOFDataController *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (instancetype)initWithArray:(NSArray*)categoriesArray{
    if (self = [self init]) {
        [self loadNewCategoriesArray:categoriesArray];
    }
    return self;
}

- (instancetype)init{
    if (self = [super init]) {
        self.categories = [NSMutableArray new];
        self.favList = [NSMutableArray new];
    }
    return self;
}

- (BOOL)loadNewCategoriesArray:(NSArray*)categoriesArray{
    self.categories = categoriesArray;
    return YES;
}

#pragma mark - Utils

- (BOOL)isCelebFaved:(WOFCelebrity*)celeb{
    __block BOOL isFaved = NO;
    [self.favList enumerateObjectsUsingBlock:^(WOFCelebrity *celebFaved, NSUInteger idx, BOOL *stop) {
        if ([celebFaved.name isEqualToString:celeb.name] && [celebFaved.locationString isEqualToString:celeb.locationString]) {
            isFaved = YES;
            *stop = YES;
        }
    }];
    return isFaved;
}

- (BOOL)addFavoriteCeleb:(WOFCelebrity*)celeb{
    BOOL faved = NO;
    if ([self isCelebFaved:celeb]) {
        [self.favList removeObject:celeb];
        faved = NO;
    }else{
        [self.favList addObject:celeb];
        faved = YES;
    }
    
    // Persist
    [[WOFDataManager sharedManager] addFavoriteCelebrity:celeb];
    return faved;
}

@end
