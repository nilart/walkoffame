//
//  CelebritiesViewController.h
//  WalkOfFame
//
//  Created by Guillermo Zafra on 04/09/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WOFDataController.h"
#import "WOFCategory.h"

@interface CelebritiesTableViewController : UITableViewController

@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UISearchDisplayController *searchBarController;
@property (nonatomic, weak) WOFCategory *category;
@property (strong, nonatomic) IBOutlet UITableView *tblContentList;

@end
