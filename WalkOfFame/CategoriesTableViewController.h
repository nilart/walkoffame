//
//  CategoriesTableViewController.h
//  WalkOfFame
//
//  Created by Guillermo Zafra on 04/09/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoriesTableViewController : UITableViewController <UISearchDisplayDelegate>

@property (nonatomic, strong) NSArray *categories;

@end
