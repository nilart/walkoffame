//
//  Celebrity.h
//  WalkOfFame
//
//  Created by Guillermo Zafra on 04/09/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GoogleMaps/GoogleMaps.h>

@interface WOFCelebrity : NSObject

- (instancetype)initWithDictionary:(NSDictionary*)dict;

@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) CLLocationCoordinate2D mapLocation;
@property (nonatomic, strong) NSString *locationString;

@end
