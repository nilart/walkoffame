//
//  Celebrity.h
//  WalkOfFame
//
//  Created by Guillermo Zafra on 31/05/16.
//  Copyright © 2016 doubleequal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Celebrity : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Celebrity+CoreDataProperties.h"
