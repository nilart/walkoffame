//
//  SettingsViewController.h
//  WalkOfFame
//
//  Created by Guillermo Zafra on 01/05/2015.
//  Copyright (c) 2015 doubleequal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (weak, nonatomic) IBOutlet UIButton *userVoiceButton;

@end
