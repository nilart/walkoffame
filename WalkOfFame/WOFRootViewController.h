//
//  WOFRootViewController.h
//  WalkOfFame
//
//  Created by Guillermo Zafra on 22/06/2015.
//  Copyright (c) 2015 doubleequal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WOFTabBarController.h"
@import GoogleMobileAds;

@interface WOFRootViewController : UIViewController <GADBannerViewDelegate, UITabBarControllerDelegate>

@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIView *bannerView;
@property (strong, nonatomic) IBOutlet WOFTabBarController *tbvc;
@end
