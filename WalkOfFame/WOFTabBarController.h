//
//  WOFTabBarViewController.h
//  WalkOfFame
//
//  Created by Guillermo Zafra on 22/06/2015.
//  Copyright (c) 2015 doubleequal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WOFTabBarController : UITabBarController

@end
