//
//  WOFDataManager.m
//  WalkOfFame
//
//  Created by Guillermo Zafra on 15/11/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import "WOFDataManager.h"
#import "WOFCategory.h"
#import "WOFCelebrity.h"
#import "Celebrity.h"

@interface WOFDataManager()

@property (nonatomic, strong) NSArray *dataCache;

@end

@implementation WOFDataManager

#pragma mark - Init

+ (instancetype)sharedManager {
    static WOFDataManager *_sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[self alloc]init];
    });
    
    return _sharedManager;
}

- (instancetype)init{
    if (self = [super init]) {
        
    }
    return self;
}

#pragma mark - Properties

- (NSArray*)categoriesData{
    if (!self.dataCache) {

    }
    return self.dataCache;
}

#pragma mark - Data Management

- (void)loadOfflineData{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:OFFLINE_DATA_FILE ofType:@"json"];
    NSError *error = nil;
    
    NSData *JSONData = [NSData dataWithContentsOfFile:filePath options:NSDataReadingMappedIfSafe error:&error];
    
    // Create an Objective-C object from JSON Data
    if (!error) {
        id JSONObject = [NSJSONSerialization
                         JSONObjectWithData:JSONData
                         options:NSJSONReadingAllowFragments
                         error:&error];
        
        NSDictionary *categoriesDict = [NSMutableDictionary new];
        
        NSArray *dataArray = (NSArray*)JSONObject;
        NSInteger counter = 0;
        for (NSDictionary *celeb in dataArray) {
            NSString *category = [celeb objectForKey:@"Category"];
            
            // Check if category exists or create it
            WOFCategory *exitingCategory = (WOFCategory*)[categoriesDict objectForKey:category];
            if (!exitingCategory) {
                exitingCategory = [[WOFCategory alloc]init];
                exitingCategory.name = category;
                [categoriesDict setValue:exitingCategory forKey:category];
                NSLog(@"Creating category: %@ ",category);
            }
            
            @try {
                id lat = [celeb objectForKey:@"Latitude"];
                id lon = [celeb objectForKey:@"Longitude"];
                if (lat && lat != [NSNull null] && lon && lon != [NSNull null]) {
                    WOFCelebrity *celebrity = [[WOFCelebrity alloc]init];
                    celebrity.name = [celeb objectForKey:@"Name"];
                    celebrity.locationString = [NSString stringWithFormat:@"%f,%f",
                                                [lat doubleValue],
                                                [lon doubleValue]];
                    [exitingCategory addCelebritiy:celebrity];
                }else{
                    NSLog(@"Celeb %@ doesn't have location!!!",[celeb objectForKey:@"Name"]);
                }

            }
            @catch (NSException *exception) {
                NSLog(@"ERROR for Celeb: %@",[celeb objectForKey:@"Name"]);
            }
            @finally {
                
            }

            counter++;
            if (counter % 500 == 0) {
                NSLog(@"Loaded: %ld",(100 * counter)/ dataArray.count);
            }
        }
        
        self.dataCache = [NSArray arrayWithArray:categoriesDict.allValues];
    }
}

- (void)addFavoriteCelebrity:(WOFCelebrity*)modelCelebrity{
    NSEntityDescription *entityLocation = [NSEntityDescription entityForName:@"Celebrity" inManagedObjectContext:self.managedObjectContext];
    NSManagedObject *managedObject = [[NSManagedObject alloc] initWithEntity:entityLocation insertIntoManagedObjectContext:self.managedObjectContext];
    
    Celebrity *celebrity;
    if([managedObject isKindOfClass:[Celebrity class]]) {
        celebrity = (Celebrity*)managedObject;
        
        celebrity.name  = modelCelebrity.name;
        celebrity.locationString = modelCelebrity.locationString;
        
        [self saveContext];
    }
}


#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.doubleequal.TestCoreData" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"WalkOfFame" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"WalkOfFame.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving and Loading support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (NSArray*)loadFavoriteCelebrities{
    // Define our table/entity to use
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Celebrity" inManagedObjectContext:self.managedObjectContext];
    
    // Setup the fetch request
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    
    // Sort by name
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [request setSortDescriptors:sortDescriptors];
    
    // Fetch the records and handle an error
    NSMutableArray *celebsArray = [NSMutableArray array];
    NSError *error;
    NSArray *fetchResults = [self.managedObjectContext executeFetchRequest:request error:&error];
    if (!error) {
        for (id object in fetchResults) {
            Celebrity *celebrity = (Celebrity*)object;
            WOFCelebrity *celebrityModel = [[WOFCelebrity alloc] init];
            celebrityModel.name = celebrity.name;
            celebrityModel.locationString = celebrity.locationString;
            [celebsArray addObject:celebrityModel];
        }
    }else{
        NSLog(@"Error fetching Employee objects: %@\n%@", [error localizedDescription], [error userInfo]);
        abort();
    }
    return celebsArray;
}


@end
