//
//  Celebrity.m
//  WalkOfFame
//
//  Created by Guillermo Zafra on 04/09/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import "WOFCelebrity.h"


#define kKeyName @"celebName"
#define kKeyLocation @"celebLocation"

@implementation WOFCelebrity

- (instancetype)initWithDictionary:(NSDictionary*)dict{
    if (self = [super init]) {
        self.locationString = [dict objectForKey:KEY_CELEBRITY_LOCATION];
        self.name = [[dict objectForKey:KEY_CELEBRITY_NAME] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    }
    return self;
}

- (void)setLocationString:(NSString *)locationString{
    if (_locationString != locationString) {
        _locationString = locationString;
        double lat = 0.0;
        double lon = 0.0;
        //@"34.101558, -118.323848"
        NSArray *components = [[locationString stringByReplacingOccurrencesOfString:@" " withString:@""] componentsSeparatedByString:@","];
        if (components.count != 2) {
            NSLog(@"Error parsing location string");
        }else{
            lat = [[components objectAtIndex:0] doubleValue];
            lon = [[components objectAtIndex:1] doubleValue];
        }
        self.mapLocation = CLLocationCoordinate2DMake(lat,lon);
    }
}

#pragma mark - Serialization

-(void)encodeWithCoder:(NSCoder *)encoder
{
    //Encode the properties of the object
    [encoder encodeObject:self.name forKey:kKeyName];
    [encoder encodeObject:self.locationString forKey:kKeyLocation];
}

-(id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if ( self != nil )
    {
        //decode the properties
        self.name = [decoder decodeObjectForKey:kKeyName];
        self.locationString = [decoder decodeObjectForKey:kKeyLocation];
    }
    return self;
}

@end
