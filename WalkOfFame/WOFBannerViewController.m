//
//  WOFBannerViewController.m
//  WalkOfFame
//
//  Created by Guillermo Zafra on 24/06/2015.
//  Copyright (c) 2015 doubleequal. All rights reserved.
//

#import "WOFBannerViewController.h"

@interface WOFBannerViewController ()

@end

@implementation WOFBannerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.bounds = CGRectMake(0, 0, 320, 50);
    
    self.bannerView = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-50, self.view.frame.size.width, 50)];
    [self.bannerView setBackgroundColor:[UIColor yellowColor]];
    [self.view addSubview:self.bannerView];
    [self.view bringSubviewToFront:self.bannerView];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self createBanner];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createBanner
{
    
    GADRequest *request = [GADRequest request];
    request.testDevices = [NSArray arrayWithObjects:@"3267521c928dfb1c7e7f41c7c6b7f2f0", @"bc5be181596f48b5f226a2554150c46f", nil];
    
    GADBannerView *bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner]; //(IS_IPAD ? kGADAdSizeSmartBannerPortrait : kGADAdSizeBanner)
    bannerView.adUnitID = @"ca-app-pub-9831779223765604/4941173495";
    bannerView.rootViewController = (id)self;
    bannerView.delegate = (id<GADBannerViewDelegate>)self;
    bannerView.frame = CGRectMake(0, 0, self.view.frame.size.width, 50);
    
    [bannerView loadRequest:request];
    
    [self.bannerView addSubview:bannerView];
    [self.view bringSubviewToFront:self.bannerView];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
