//
//  CelebritiesViewController.m
//  WalkOfFame
//
//  Created by Guillermo Zafra on 04/09/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import "CelebritiesTableViewController.h"
#import "WOFCelebrity.h"
#import "WOFTableViewCell.h"
#import "MapViewController.h"

@interface CelebritiesTableViewController ()

@property (nonatomic, strong) NSMutableArray *filteredCelebrities;
@property (nonatomic, assign) BOOL isSearching;
@end

@implementation CelebritiesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //    [self.tableView registerClass:[WOFTableViewCell class] forCellReuseIdentifier:@"CelebrityCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"WOFTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"CelebrityCell"];
    
    // Do any additional setup after loading the view.
    self.filteredCelebrities = [NSMutableArray new];
    
    [self.tableView setContentInset:UIEdgeInsetsMake(0,0,50,0)];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.searchBarController.searchResultsTableView == tableView) {
        return [self.filteredCelebrities count];
    } else {
        return [self.category.celebrities count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"CelebrityCell";
    
    WOFCelebrity *celeb;
    
    if (self.searchBarController.searchResultsTableView == tableView) {
        celeb = [self.filteredCelebrities objectAtIndex:indexPath.row];
    } else {
        celeb = [self.category.celebrities objectAtIndex:indexPath.row];
    }
    
    WOFTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[WOFTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.celeb = celeb;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    WOFCelebrity *celeb;
    
    if (self.searchBarController.searchResultsTableView == tableView) {
        celeb = [self.filteredCelebrities objectAtIndex:indexPath.row];
    } else {
        celeb = [self.category.celebrities objectAtIndex:indexPath.row];
    }
    
    [[WOFDataController sharedController] setSelectedCelebrity:celeb];
    
    // Show map 
    MapViewController *mapView = [self.storyboard instantiateViewControllerWithIdentifier:@"MapScene"];
    mapView.isModal = YES;
    [self presentViewController:mapView animated:YES completion:nil];
}

#pragma mark - UISearchBarDelegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    self.isSearching = YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSLog(@"Text change - %d",self.isSearching);
    
    //Remove all objects first.
    [self.filteredCelebrities removeAllObjects];
    
    if([searchText length] != 0) {
        self.isSearching = YES;
        [self searchTableList];
    }
    else {
        self.isSearching = NO;
    }
    [self.tblContentList reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"Cancel clicked");
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"Search Clicked");
    [self searchTableList];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
//    self.isSearching = NO;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller
shouldReloadTableForSearchString:(NSString *)searchString
{
//    [self searchTableList];
    
    return YES;
}

#pragma mark - Content Filtering


- (void)searchTableList {
    NSString *searchString = self.searchBar.text;
    for (WOFCelebrity *celeb in self.category.celebrities) {
        NSComparisonResult result = [celeb.name compare:searchString options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchString length])];
        if (result == NSOrderedSame) {
            [self.filteredCelebrities addObject:celeb];
        }
    }
}


@end
