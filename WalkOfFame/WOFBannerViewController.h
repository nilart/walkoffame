//
//  WOFBannerViewController.h
//  WalkOfFame
//
//  Created by Guillermo Zafra on 24/06/2015.
//  Copyright (c) 2015 doubleequal. All rights reserved.
//

#import <UIKit/UIKit.h>
@import GoogleMobileAds;

@interface WOFBannerViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIView *bannerView;

@end
