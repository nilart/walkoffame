//
//  Category.m
//  WalkOfFame
//
//  Created by Guillermo Zafra on 04/09/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import "WOFCategory.h"

@implementation WOFCategory

- (instancetype)init{
    if (self = [super init]) {
        self.celebrities = [NSMutableArray new];
    }
    return self;
}

- (instancetype)initWithDict:(NSDictionary*)dict{
    if (self = [super init]) {
        for (NSDictionary *celebrityDict in [dict objectForKey:KEY_CATEGORY_CELEBRITIES]) {
            WOFCelebrity *celebrity  = [[WOFCelebrity alloc] initWithDictionary:celebrityDict];
            [self addCelebritiy:celebrity];
        }
        self.name = [dict objectForKey:KEY_CATEGORY_NAME];
    }
    return self;
}

- (id)objectAtIndex:(NSUInteger)index{
    return [self.celebrities objectAtIndex:index];
}

- (void)addCelebritiy:(WOFCelebrity*)celeb{
    [self.celebrities addObject:celeb];
}

@end
