//
//  AppDelegate.h
//  WalkOfFame
//
//  Created by Guillermo Zafra on 01/09/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WOFDataController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) WOFDataController *dataController;

@end

