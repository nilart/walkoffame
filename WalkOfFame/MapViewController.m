//
//  FirstViewController.m
//  WalkOfFame
//
//  Created by Guillermo Zafra on 01/09/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import "MapViewController.h"
#import "WOFDataController.h"
#import "WOFCelebrity.h"
#import "WOFCategory.h"
#import "CCHMapClusterAnnotation.h"
#import "ClusterAnnotationView.h"



#define kDefaultInitCoords CLLocationCoordinate2DMake(34.1011349, -118.3422383)

@interface MapViewController ()

//@property (nonatomic, strong) GMSMapView *mapView_;
@property (nonatomic, weak) WOFDataController *dataController;
@property (strong, nonatomic) CCHMapClusterController *mapClusterController;
@property (nonatomic, strong) UIButton *backBtn;
@property (nonatomic, strong) UIButton *cancelFilterBtn;

@property (nonatomic, strong) UIButton *locateUser;
@property (nonatomic, strong) UIButton *cancelNavigationButton;

@property (nonatomic, strong) UILabel *statusLabel;
@property (nonatomic, assign) MapViewMode mapViewMode;

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation* currentLocation;

@property (nonatomic, assign) BOOL isDisplayingRoute;
@property (nonatomic, strong) NSMutableArray *routeLines;
@end

@implementation MapViewController
            
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.dataController = [WOFDataController sharedController];
    
    MKUserTrackingBarButtonItem *buttonItem = [[MKUserTrackingBarButtonItem alloc] initWithMapView:self.mapView];
    self.navigationItem.rightBarButtonItem = buttonItem;
    
    self.mapView.delegate = self;
    self.mapView.showsUserLocation=YES;
    self.mapView.rotateEnabled = NO;
    
    self.routeLines = [NSMutableArray new];
    
    [self.locationManager requestWhenInUseAuthorization];
    if ([CLLocationManager locationServicesEnabled] )
    {
        if (self.locationManager == nil )
        {
            self.locationManager = [[CLLocationManager alloc] init];
            self.locationManager.delegate = self;
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            self.locationManager.distanceFilter = kCLDistanceFilterNone; //kCLDistanceFilterNone// kDistanceFilter;
        }
        
        [self.locationManager startUpdatingLocation];
    }
    
    // Add button to cancel filter
    self.cancelFilterBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.cancelFilterBtn.frame = CGRectMake(self.view.frame.size.width - 40, 30, 30, 30);
    [self.cancelFilterBtn setBackgroundImage:[UIImage imageNamed:@"clearFilter"] forState:UIControlStateNormal];
    [self.cancelFilterBtn addTarget:self action:@selector(cancelSearch) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.cancelFilterBtn];
    
    NSMutableArray *annotationsArray;
    WOFCelebrity *celebrity = self.dataController.selectedCelebrity;
    CLLocationCoordinate2D coords;
    if (celebrity) {
        coords = celebrity.mapLocation;
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
        point.coordinate = celebrity.mapLocation;
        point.title = celebrity.name;
        [self.mapView addAnnotation:point];
        annotationsArray = [NSMutableArray arrayWithArray:@[point]];
        
        self.mapViewMode = MapViewModeSelected;
    }else if(self.favoritesMode){
        annotationsArray = [self loadMarkersFromFavorites];
        coords = [annotationsArray[0] coordinate];
        self.mapViewMode = MapViewModeFavorites;
    }else{
        annotationsArray = [self loadMarkers];
        // Creates a default marker.
        coords = kDefaultInitCoords;
        self.mapViewMode = MapViewModeAll;
        
        [self.cancelFilterBtn setHidden:YES];
    }
    
    self.locateUser = [UIButton buttonWithType:UIButtonTypeCustom];
    self.locateUser.frame = CGRectMake(15, 30, 30, 30);
    [self.locateUser setBackgroundImage:[UIImage imageNamed:@"locateMe"] forState:UIControlStateNormal];
    [self.locateUser addTarget:self action:@selector(locateMe) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.locateUser];
    
    self.cancelNavigationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.cancelNavigationButton.frame = CGRectMake(15, 75, 30, 30);
    [self.cancelNavigationButton setBackgroundImage:[UIImage imageNamed:@"cancelNavigation"] forState:UIControlStateNormal];
    [self.cancelNavigationButton addTarget:self action:@selector(cancelNavigation) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.cancelNavigationButton];
    [self.cancelNavigationButton setHidden:YES];
    
    if (self.isModal) {
        // Add back button
        self.backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.backBtn.frame = CGRectMake(15, self.view.frame.size.height-45, 30, 30);
        [self.backBtn setBackgroundImage:[UIImage imageNamed:@"home.png"] forState:UIControlStateNormal];
        [self.backBtn addTarget:self action:@selector(backHome) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.backBtn];
    }
    
    
    self.statusLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 45, 220, 20)];
    self.statusLabel.center = CGPointMake(self.view.frame.size.width/2, 45);
    self.statusLabel.text = [self statusTextForViewMode:self.mapViewMode];
    self.statusLabel.font=[self.statusLabel.font fontWithSize:12];
    self.statusLabel.backgroundColor = [UIColor whiteColor];
    self.statusLabel.textColor = [UIColor blueColor];
    self.statusLabel.textAlignment = NSTextAlignmentCenter;
    self.statusLabel.viewForBaselineLayout.layer.cornerRadius = 8;
    self.statusLabel.layer.masksToBounds = YES;
    [self.view addSubview:self.statusLabel];

    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(coords, self.view.bounds.size.width,self.view.bounds.size.height);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
    [self renderMarkers:annotationsArray];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)renderMarkers:(NSArray*)annotationsArray{
    self.mapClusterController = [[CCHMapClusterController alloc] initWithMapView:self.mapView];
    self.mapClusterController.delegate = self;
    self.mapClusterController.cellSize = 60;
    self.mapClusterController.maxZoomLevelForClustering = 18.0;
    
    
    [self.mapClusterController setDebuggingEnabled:NO];
    [self.mapClusterController addAnnotations:annotationsArray withCompletionHandler:^{
        NSLog(@"Clustering completed");
    }];
}

- (NSMutableArray*)loadMarkers{
    NSMutableArray *annotationsArray = [NSMutableArray new];
    
    for (WOFCategory *category in self.dataController.categories) {
        for (WOFCelebrity *celeb in category.celebrities) {
            
            MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
            point.coordinate = celeb.mapLocation;
            point.title = celeb.name;
            point.subtitle = category.name;
            
            [annotationsArray addObject:point];
        }
    }
    return annotationsArray;
}

- (NSMutableArray*)loadMarkersFromFavorites{
    NSMutableArray *annotationsArray = [NSMutableArray new];
    
    for (WOFCategory *category in self.dataController.categories) {
        for (WOFCelebrity *celeb in category.celebrities) {
            if ([self.dataController isCelebFaved:celeb]) {
                MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
                point.coordinate = celeb.mapLocation;
                point.title = celeb.name;
                point.subtitle = category.name;
                
                
                [annotationsArray addObject:point];
            }

        }
    }
    return annotationsArray;
}

- (void)cancelSearch{
    [self.cancelFilterBtn setHidden:YES];
    NSMutableArray *annotationsArray = [self loadMarkers];
    [self renderMarkers:annotationsArray];
    
    self.mapViewMode = MapViewModeAll;
    self.statusLabel.text = [self statusTextForViewMode:self.mapViewMode];
}

- (void)cancelNavigation{
    self.isDisplayingRoute = NO;
    for (id polyLine in self.routeLines) {
        [self.mapView removeOverlay:polyLine];
    }
}

- (void)backHome{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)locateMe{
    [self.locationManager requestWhenInUseAuthorization];
    if (self.currentLocation.coordinate.latitude != 0 &&
        self.currentLocation.coordinate.longitude != 0) {
        [self.mapView setCenterCoordinate:self.currentLocation.coordinate animated:YES];
    }
}

- (NSString*)statusTextForViewMode:(MapViewMode)viewMode{
    switch (viewMode) {
        case MapViewModeAll:
            return @"Showing all celebrities";
            break;
        case MapViewModeFavorites:
            return @"Showing only favorites";
            break;
        case MapViewModeSelected:
            return @"Showing selected celebrity";
            break;
        default:
            return @"";
            break;
    }
}

- (void)setIsDisplayingRoute:(BOOL)isDisplayingRoute{
    if (_isDisplayingRoute != isDisplayingRoute) {
        _isDisplayingRoute = isDisplayingRoute;
        
        if (isDisplayingRoute) {
            [self.cancelNavigationButton setHidden:NO];
        }else{
            [self.cancelNavigationButton setHidden:YES];
        }
        
    }
}

#pragma mark - Delegate

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    self.currentLocation = [locations lastObject];
    // here we get the current location
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    MKAnnotationView *annotationView;
    
    if (annotation == self.mapView.userLocation) return nil;
    
    if ([annotation isKindOfClass:[CCHMapClusterAnnotation class]]) {
        
        CCHMapClusterAnnotation *clusterAnnotation = (CCHMapClusterAnnotation *)annotation;
        if (clusterAnnotation.annotations.count == 1) {
            annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:[clusterAnnotation.annotations anyObject] reuseIdentifier:@"loc"];
            
            UIButton *directionButton = [UIButton buttonWithType:UIButtonTypeCustom];
            UIImage *directionIcon = [UIImage imageNamed:@"navigate"];
            directionButton.frame = CGRectMake(0, 0, directionIcon.size.width, directionIcon.size.height);
            
            [directionButton setImage:directionIcon forState:UIControlStateNormal];
            [directionButton setImage:[UIImage imageNamed:@"navigate_sel"] forState:UIControlStateSelected];
            
            annotationView.rightCalloutAccessoryView = directionButton;
        }else{
            annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"loc"];
        }
        
//        
//        CCHMapClusterAnnotation *clusterAnnotation = (CCHMapClusterAnnotation *)annotation;
        annotationView.canShowCallout = YES;
    }else{
        annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"loc"];
    }
    annotationView.canShowCallout = YES;
    
    return annotationView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    [self.locationManager requestWhenInUseAuthorization];
    [self cancelNavigation];
    
    __block UIButton *navigateButton = (UIButton*)view.rightCalloutAccessoryView;
    [navigateButton setImage:nil forState:UIControlStateNormal];
    
    __block UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    CGFloat halfButtonHeight = navigateButton.bounds.size.height / 2;
    CGFloat buttonWidth = navigateButton.bounds.size.width;
    indicator.center = CGPointMake(buttonWidth - halfButtonHeight , halfButtonHeight);
    [navigateButton addSubview:indicator];
    [indicator startAnimating];
    
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
    [request setSource:[MKMapItem mapItemForCurrentLocation]];
    
    MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:view.annotation.coordinate addressDictionary:nil];
    MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
    [mapItem setName:view.annotation.subtitle];
    
//    [mapItem openInMapsWithLaunchOptions:nil]; // To open in maps app
    
    [request setDestination:mapItem];
    [request setTransportType:MKDirectionsTransportTypeWalking]; // This can be limited to automobile and walking directions.
    [request setRequestsAlternateRoutes:YES]; // Gives you several route options.
    MKDirections *directions = [[MKDirections alloc] initWithRequest:request];
    [directions calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
        [indicator stopAnimating];
        [indicator removeFromSuperview];
        [navigateButton setImage:[UIImage imageNamed:@"navigate"] forState:UIControlStateNormal];
        if (!error) {
            self.isDisplayingRoute = YES;
            for (MKRoute *route in [response routes]) {
                [self.routeLines addObject:[route polyline]];
                [self.mapView addOverlay:[route polyline] level:MKOverlayLevelAboveRoads]; // Draws the route above roads, but below labels.
            }
        }else{
            // ERROR
        }
    }];
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay
{
    if ([overlay isKindOfClass:[MKPolyline class]]) {
        MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
        [renderer setStrokeColor:[UIColor blueColor]];
        [renderer setLineWidth:5.0];
        return renderer;
    }
    return nil;
}

- (void)mapClusterController:(CCHMapClusterController *)mapClusterController willReuseMapClusterAnnotation:(CCHMapClusterAnnotation *)mapClusterAnnotation
{
    ClusterAnnotationView *clusterAnnotationView = (ClusterAnnotationView *)[self.mapView viewForAnnotation:mapClusterAnnotation];
    clusterAnnotationView.canShowCallout = YES;
}

- (NSString *)mapClusterController:(CCHMapClusterController *)mapClusterController titleForMapClusterAnnotation:(CCHMapClusterAnnotation *)mapClusterAnnotation
{
    NSUInteger numAnnotations = mapClusterAnnotation.annotations.count;
    NSString *unit = numAnnotations > 1 ? @"celebrities" : @"celebrity";
    return [NSString stringWithFormat:@"%tu %@", numAnnotations, unit];
}

- (NSString *)mapClusterController:(CCHMapClusterController *)mapClusterController subtitleForMapClusterAnnotation:(CCHMapClusterAnnotation *)mapClusterAnnotation
{
    NSUInteger numAnnotations = MIN(mapClusterAnnotation.annotations.count, 5);
    NSArray *annotations = [mapClusterAnnotation.annotations.allObjects subarrayWithRange:NSMakeRange(0, numAnnotations)];
    NSArray *titles = [annotations valueForKey:@"title"];
    return [titles componentsJoinedByString:@", "];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
