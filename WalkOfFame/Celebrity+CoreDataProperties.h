//
//  Celebrity+CoreDataProperties.h
//  WalkOfFame
//
//  Created by Guillermo Zafra on 31/05/16.
//  Copyright © 2016 doubleequal. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Celebrity.h"

NS_ASSUME_NONNULL_BEGIN

@interface Celebrity (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *locationString;

@end

NS_ASSUME_NONNULL_END
