//
//  WOFTableViewCell.m
//  WalkOfFame
//
//  Created by Guillermo Zafra on 15/04/2015.
//  Copyright (c) 2015 doubleequal. All rights reserved.
//

#import "WOFTableViewCell.h"
#import "WOFDataController.h"

@implementation WOFTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        // Helpers
       
        // Initialize Main Label
//        self.mainLabel = [[UILabel alloc] initWithFrame:CGRectMake(8.0, 8.0, size.width - 16.0, size.height - 16.0)];
//        
////         Configure Main Label
//        [self.mainLabel setFont:[UIFont boldSystemFontOfSize:24.0]];
//        [self.mainLabel setTextAlignment:NSTextAlignmentCenter];
//        [self.mainLabel setTextColor:[UIColor orangeColor]];
//        [self.mainLabel setAutoresizingMask:(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
//        
//        // Add Main Label to Content View
//        [self.contentView addSubview:self.mainLabel];
    }
    
    return self;
}

- (void)setCeleb:(WOFCelebrity *)celeb{
    if (_celeb != celeb) {
        _celeb = celeb;
        
        self.mainLabel.text = celeb.name;
        
        if ([[WOFDataController sharedController] isCelebFaved:self.celeb]) {
            self.favButton.selected = YES;
        }else{
            self.favButton.selected = NO;
        }
    }
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(IBAction)favButtonPressed:(id)sender{
    
    if ([[WOFDataController sharedController] isCelebFaved:self.celeb]) {
        self.favButton.selected = NO;
    }else{
        self.favButton.selected = YES;
    }
    
    // Reload parent table view data (Not needed for now)
//    [[self parentTableView] reloadData];
    
    [[WOFDataController sharedController] addFavoriteCeleb:self.celeb];
}

-(UITableView *) parentTableView {
    // iterate up the view hierarchy to find the table containing this cell/view
    UIView *aView = self.superview;
    while(aView != nil) {
        if([aView isKindOfClass:[UITableView class]]) {
            return (UITableView *)aView;
        }
        aView = aView.superview;
    }
    return nil; // this view is not within a tableView
}

@end
