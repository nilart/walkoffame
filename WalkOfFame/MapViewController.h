//
//  FirstViewController.h
//  WalkOfFame
//
//  Created by Guillermo Zafra on 01/09/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "CCHMapClusterController.h"
#import "CCHMapClusterControllerDelegate.h"
#import <CoreLocation/CoreLocation.h>

typedef enum : NSUInteger {
    MapViewModeAll,
    MapViewModeSelected,
    MapViewModeFavorites
} MapViewMode;

@interface MapViewController : UIViewController <MKMapViewDelegate,CCHMapClusterControllerDelegate, CLLocationManagerDelegate>

@property (nonatomic, strong) IBOutlet MKMapView *mapView;
@property (nonatomic, assign) BOOL isModal;
@property (nonatomic, assign) BOOL favoritesMode;
@end

