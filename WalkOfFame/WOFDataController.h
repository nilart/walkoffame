//
//  DataController.h
//  WalkOfFame
//
//  Created by Guillermo Zafra on 04/09/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import <Foundation/Foundation.h>

@class WOFCelebrity;

@interface WOFDataController : NSObject

@property (nonatomic, strong) NSArray *categories;
@property (nonatomic, weak) WOFCelebrity *selectedCelebrity;
@property (nonatomic, strong) NSMutableArray *favList;

+ (instancetype)sharedController;
- (instancetype)initWithArray:(NSArray*)categoriesArray;
- (BOOL)loadNewCategoriesArray:(NSArray*)categoriesArray;
- (BOOL)isCelebFaved:(WOFCelebrity*)celeb;
- (BOOL)addFavoriteCeleb:(WOFCelebrity*)celeb;

@end
