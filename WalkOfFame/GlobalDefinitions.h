//
//  GlobalDefinitions.h
//  WalkOfFame
//
//  Created by Guillermo Zafra on 15/11/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#ifndef WalkOfFame_GlobalDefinitions_h
#define WalkOfFame_GlobalDefinitions_h

// Keys

#define KEY_VERSION @"Version"
#define KEY_DATA @"Data"
#define KEY_CATEGORY_CELEBRITIES @"CategoryCelebrities"
#define KEY_CATEGORY_NAME @"CategoryName"
#define KEY_CELEBRITIY_ADDRESS @"CelebrityAddress"
#define KEY_CELEBRITY_NAME @"CelebrityName"
#define KEY_CELEBRITY_LOCATION @"CelebrityLocation"


#define OFFLINE_DATA_FILE @"offlineData"

// Macros
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

#endif
