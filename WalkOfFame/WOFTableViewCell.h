//
//  WOFTableViewCell.h
//  WalkOfFame
//
//  Created by Guillermo Zafra on 15/04/2015.
//  Copyright (c) 2015 doubleequal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WOFCelebrity.h"

@interface WOFTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *mainLabel;
@property (weak, nonatomic) IBOutlet UIButton *favButton;
@property (weak, nonatomic) WOFCelebrity *celeb;
-(IBAction)favButtonPressed:(id)sender;

@end
