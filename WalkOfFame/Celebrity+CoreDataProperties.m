//
//  Celebrity+CoreDataProperties.m
//  WalkOfFame
//
//  Created by Guillermo Zafra on 31/05/16.
//  Copyright © 2016 doubleequal. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Celebrity+CoreDataProperties.h"

@implementation Celebrity (CoreDataProperties)

@dynamic name;
@dynamic locationString;

@end
