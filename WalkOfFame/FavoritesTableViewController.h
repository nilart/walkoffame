//
//  FavoritesTableViewController.h
//  WalkOfFame
//
//  Created by Guillermo Zafra on 24/04/2015.
//  Copyright (c) 2015 doubleequal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavoritesTableViewController : UITableViewController

@end
