//
//  WOFRootViewController.m
//  WalkOfFame
//
//  Created by Guillermo Zafra on 22/06/2015.
//  Copyright (c) 2015 doubleequal. All rights reserved.
//

#import "WOFRootViewController.h"


@interface WOFRootViewController ()

@end

@implementation WOFRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"Google Mobile Ads SDK version: %@", [GADRequest sdkVersion]);
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self openMainView];
    
    [self createBanner];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)openMainView{
    WOFTabBarController *tbvc =[self.storyboard instantiateViewControllerWithIdentifier:@"MainTabBar"];
    UITabBar *tabBar = tbvc.tabBar;
    UITabBarItem *tabBarItem1 = [tabBar.items objectAtIndex:0];
    UITabBarItem *tabBarItem2 = [tabBar.items objectAtIndex:1];
    UITabBarItem *tabBarItem3 = [tabBar.items objectAtIndex:2];
//    UITabBarItem *tabBarItem4 = [tabBar.items objectAtIndex:3];
    
    (void)[tabBarItem1 initWithTitle:@"Celebrities" image:[UIImage imageNamed:@"celebrities.png"] selectedImage:[UIImage imageNamed:@"celebrities_sel.png"]];
    (void)[tabBarItem2 initWithTitle:@"Map" image:[UIImage imageNamed:@"maps.png"] selectedImage:[UIImage imageNamed:@"maps_sel.png"]];
    (void)[tabBarItem3 initWithTitle:@"Favorites" image:[UIImage imageNamed:@"favorites.png"] selectedImage:[UIImage imageNamed:@"favorites_sel.png"]];
//    (void)[tabBarItem4 initWithTitle:@"Settings" image:[UIImage imageNamed:@"settings.png"] selectedImage:[UIImage imageNamed:@"settings_sel.png"]];
    tbvc.tabBar.frame = self.mainView.frame;
    tbvc.view.autoresizesSubviews = YES;
    tbvc.view.autoresizingMask=UIViewAutoresizingFlexibleWidth;
    [self presentViewController:tbvc animated:NO completion:nil];
    self.tbvc = tbvc;
}

- (void)createBanner
{
    GADRequest *request = [GADRequest request];
    request.testDevices = [NSArray arrayWithObjects:@"3267521c928dfb1c7e7f41c7c6b7f2f0", nil];
    
    GADBannerView *bannerView = [[GADBannerView alloc] initWithAdSize:IS_IPAD ? kGADAdSizeLeaderboard : kGADAdSizeBanner];
    bannerView.adUnitID = @"ca-app-pub-9831779223765604/4941173495";
    bannerView.rootViewController = (id)self;
    bannerView.delegate = (id<GADBannerViewDelegate>)self;
    bannerView.frame = CGRectMake(0, self.tbvc.view.frame.size.height- self.tbvc.tabBar.frame.size.height- 50, self.view.frame.size.width, 50);
    
    [bannerView loadRequest:request];
    
    [self.tbvc.view addSubview:bannerView];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



#pragma mark - GAD Banner View Delegate

- (void)adViewDidReceiveAd:(GADBannerView *)view{}

- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error{}

- (void)adViewWillPresentScreen:(GADBannerView *)adView{}

- (void)adViewWillDismissScreen:(GADBannerView *)adView{}

- (void)adViewDidDismissScreen:(GADBannerView *)adView{}

- (void)adViewWillLeaveApplication:(GADBannerView *)adView{}

@end
