//
//  AppDelegate.m
//  WalkOfFame
//
//  Created by Guillermo Zafra on 01/09/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import "AppDelegate.h"
#import <GoogleMaps/GoogleMaps.h>
#import "CategoriesTableViewController.h"
#import "WOFDataManager.h"

@interface AppDelegate ()

@end

@implementation AppDelegate
            

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [GMSServices provideAPIKey:@"AIzaSyCqwxZwlbWZrW6gU30TK4BkPIbZS0meda4"];
    
//    UITabBarController  *tabBarController = (UITabBarController *)self.window.rootViewController;
//    UINavigationController *navigationController = [[(UINavigationController*)tabBarController viewControllers] objectAtIndex:0];
////    CategoriesTableViewController *rootViewController = (CategoriesTableViewController *)[[navigationController viewControllers]objectAtIndex:0];
    
    WOFDataController *controller = [WOFDataController sharedController];
    [[WOFDataManager sharedManager] loadOfflineData];
    [controller loadNewCategoriesArray:[[WOFDataManager sharedManager] categoriesData]];
    self.dataController = controller;
    
    // Load favs from user defaults
    controller.favList = [NSMutableArray arrayWithArray:[[WOFDataManager sharedManager] loadFavoriteCelebrities]];

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
