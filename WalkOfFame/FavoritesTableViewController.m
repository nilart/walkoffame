//
//  FavoritesTableViewController.m
//  WalkOfFame
//
//  Created by Guillermo Zafra on 24/04/2015.
//  Copyright (c) 2015 doubleequal. All rights reserved.
//

#import "FavoritesTableViewController.h"
#import "WOFTableViewCell.h"
#import "WOFDataController.h"
#import "MapViewController.h"


@interface FavoritesTableViewController ()

@property (nonatomic, strong) NSArray *favorites;
@property (nonatomic, strong) UIButton *goToMapButton;

@end

@implementation FavoritesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"WOFTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"CelebrityCell"];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.favorites = [WOFDataController sharedController].favList;
    
    self.tableView.contentInset = UIEdgeInsetsMake(80.0f, 0.0f, 0.0f, 0.0f);
    
    self.goToMapButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.goToMapButton.titleLabel.text = @"Show on Map";
    [self.goToMapButton addTarget:self
               action:@selector(showOnMap)
     forControlEvents:UIControlEventTouchUpInside];
    [self.goToMapButton setTitle:@"Show on Map" forState:UIControlStateNormal];
    self.goToMapButton.frame = CGRectMake(80.0, -40.0, 160.0, 40.0);
    self.goToMapButton.center = CGPointMake(self.view.center.x, self.goToMapButton.center.y);
    [self.view addSubview:self.goToMapButton];
}

- (void)viewWillAppear:(BOOL)animated{
    [self.tableView reloadData];
    if (self.favorites.count) {
        self.goToMapButton.enabled = YES;
    }else{
        self.goToMapButton.enabled = NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (void)showOnMap{
    // Show map
    MapViewController *mapView = [self.storyboard instantiateViewControllerWithIdentifier:@"MapScene"];
    mapView.favoritesMode = YES;
    mapView.isModal = YES;
    [self presentViewController:mapView animated:YES completion:nil];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.favorites count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"CelebrityCell";
    WOFCelebrity *celeb = nil;
    if (self.favorites.count > indexPath.row) {
        celeb =[self.favorites objectAtIndex:indexPath.row];
    }
    
    WOFTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[WOFTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.celeb = celeb;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    WOFCelebrity *celeb =[self.favorites objectAtIndex:indexPath.row];
    
    [[WOFDataController sharedController] setSelectedCelebrity:celeb];
    

    MapViewController *mapView = [self.storyboard instantiateViewControllerWithIdentifier:@"MapScene"];
    mapView.isModal = YES;
    [self presentViewController:mapView animated:YES completion:nil];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
