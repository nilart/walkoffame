//
//  Category.h
//  WalkOfFame
//
//  Created by Guillermo Zafra on 04/09/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WOFCelebrity.h"

@interface WOFCategory : NSObject

@property (nonatomic, strong) NSMutableArray *celebrities;
@property (nonatomic, strong) NSString *name;

- (void)addCelebritiy:(WOFCelebrity*)celeb;
- (id)objectAtIndex:(NSUInteger)index;
- (instancetype)initWithDict:(NSDictionary*)dict;

@end
