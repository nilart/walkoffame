//
//  WOFDataManager.h
//  WalkOfFame
//
//  Created by Guillermo Zafra on 15/11/14.
//  Copyright (c) 2014 doubleequal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WOFCelebrity.h"
#import <CoreData/CoreData.h>

@interface WOFDataManager : NSObject

+ (instancetype)sharedManager;
- (void)loadOfflineData;
- (void)addFavoriteCelebrity:(WOFCelebrity*)modelCelebrity;
- (NSArray*)loadFavoriteCelebrities;
- (void)saveContext;

@property (nonatomic) NSArray *categoriesData;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end
