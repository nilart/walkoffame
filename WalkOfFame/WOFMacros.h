//
//  WOFMacros.h
//  WalkOfFame
//
//  Created by Guillermo Zafra on 24/06/2015.
//  Copyright (c) 2015 doubleequal. All rights reserved.
//

#ifndef WalkOfFame_WOFMacros_h
#define WalkOfFame_WOFMacros_h

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

#endif
